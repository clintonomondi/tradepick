<?php
require_once ('navbar.php')
?>


<!--Page Title-->
<section class="page-title" style="background-image:url(/pics/products/bg.jpeg)">
    <div class="auto-container">
        <div class="content">
            <h1>About <span>Us</span></h1>
            <ul class="page-breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Pages</li>
                <li>About Us</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Services Section Three-->
<section class="services-section-three">
    <div class="auto-container">
        <div class="row clearfix">

            <!-- Services Block -->
            <div class="services-block-three style-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow bounceIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="icon-box">
                        <span class="icon flaticon-bar-chart"></span>
                    </div>
                    <h6><a href="#">Our Vision</a></h6>
                    <div class="text">To became the most excellent source of satisfaction by offering products of indisputable quality to our customers in Kenya and beyond.</div>
                </div>
            </div>

            <!-- Services Block -->
            <div class="services-block-three style-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow bounceIn" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="icon-box">
                        <span class="icon flaticon-board"></span>
                    </div>
                    <h6><a href="#">Our Mission</a></h6>
                    <div class="text">We develop a mirror that gives utmost satisfaction to <br>our customers by creating<br> value to their needs.r.</div>
                </div>
            </div>

            <!-- Services Block -->
            <div class="services-block-three style-two col-lg-4 col-md-12 col-sm-12">
                <div class="inner-box wow bounceIn" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="icon-box">
                        <span class="icon flaticon-student"></span>
                    </div>
                    <h6><a href="#">Core values</a></h6>
                    <div class="text">We understand that everyone’s requirement is different so we provide a diverse range of products to fit all requirements.</div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- About Section Four -->
<section class="about-section-four">
    <div class="auto-container">

        <!-- Sec Title -->
        <div class="sec-title centered">
            <div class="title">about us</div>
            <h2>Our team aspires to bring you the world’s   <br> best energy drink and <span>share the drink life we love</span></h2>
        </div>

        <div class="row clearfix">

            <!-- Content Column -->
            <div class="image-column col-lg-5 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-1"><img src="/pics/products/p14.jpeg" alt=""></div>
                    <div class="image-2"><img src="/pics/products/p7.jpeg" alt=""></div>
                </div>
            </div>

            <!-- Skills Column -->
            <div class="skills-column col-lg-7 col-md-12 col-sm-12">
                <div class="inner-column">

                    <!--Skills-->


                    <div class="text">
                        <p>Tradepick Investments Limited is a registered company in Kenya under the Companies Act 2015.

                            The company was established to provide quality products and services to organizations and companies in the most efficient way.</p>
                        <p>We are customer-focused in all areas of our operations and provide excellent, dependable and reliable supplies to clientele both in the public and private sector. With the vision, experience and technical know how, we are well able to handle both small and large supplies.</p>
                        <p>Tradepick Investments Limited strive to provide the best possible service to all clients. Our staff understand that quality is the cornerstone to maintaining successful client relationships and delivering customer satisfaction. Our management and technical team is superb. We have highly qualified and experienced technical team that is capable of delivering the best customer experience and thus maintain our company client relationships..</p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<!-- End About Section Four -->

















<?php
require_once ('footer.php')
?>
