<!--Main Footer-->
<footer class="main-footer">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">

                <!--Column-->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo">
                                    <a href="/"><img src="/pics/logo2.png" alt="" /></a>
                                </div>
                                <div class="text">We understand that everyone’s requirement is different so we provide a diverse range of products to fit all requirements. Our core business is to offer general supplies.</div>

                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-lg-5 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h4>Links</h4>
                                <ul class="list-link">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Column-->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h4>Support</h4>
                                <ul class="list-style-two">
                                    <li><span class="icon fa fa-phone"></span>+254 722338239</li>
                                    <li><span class="icon fa fa-envelope"></span> info@tradepickinvetsments.com</li>
                                    <li><span class="icon fa fa-home"></span>National Cereals and Produce Board warehouses Store 13,Enterprise road next to Dt dobie. Po box 51759-00100 Nairobi </li>
                                </ul>
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget gallery-widget">
                                <h4>Other Products</h4>
                                <div class="widget-content">
                                    <div class="images-outer clearfix">
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="/pics/products/p4.jpeg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="/pics/products/p4.jpeg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="/pics/products/p7.jpeg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="/pics/products/p7.jpeg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="/pics/products/p5.jpeg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="/pics/products/p5.jpeg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="/pics/products/p9.jpeg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="/pics/products/p9.jpeg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="/pics/products/milk2.jpeg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="/pics/products/milk2.jpeg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="/pics/products/milk.jpeg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="/pics/products/milk.jpeg" alt=""></a></figure>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Copyright Column -->
                <div class="copyright-column col-lg-6 col-md-6 col-sm-12">
                    <div class="copyright">Copy Right 2020 &copy;  All rights reserved  <a href="#">Tradepick Investment</a></div>
                </div>

                <!-- Social Column -->
                <div class="social-column col-lg-6 col-md-6 col-sm-12">
                    <ul>
                        <li class="follow">Follow us: </li>
                        <li><a href="#"><span class="fa fa-facebook-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-linkedin-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-google-plus-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-rss-square"></span></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>

</div>
<!--End pagewrapper-->

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sticky.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/owl.js"></script>
<script src="js/mixitup.js"></script>
<script src="js/wow.js"></script>
<script src="js/validate.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/main.js"></script>
<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDTPlX-43R1TpcQUyWjFgiSfL_BiGxslZU"></script>
<script src="js/map-script.js"></script>
<!--End Google Map APi-->
<script src="/js/api.js"></script>

</body>

<!-- Mirrored from html.themexriver.com/finano/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Dec 2020 04:00:32 GMT -->
</html>