<!DOCTYPE html>
<html>

<!-- Mirrored from html.themexriver.com/finano/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Dec 2020 04:00:03 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="description" content="We have something special for your refined taste">
    <title>Tradepick Investment</title>
    <!-- Stylesheets -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="/pics/logo.PNG" type="image/x-icon">
    <script src="/alert/alertify.min.js"></script>
    <link rel="stylesheet" href="/alert/css/alertify.min.css" />
    <link rel="stylesheet" href="/alert/css/themes/default.min.css" />
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<body>
<div class="page-wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header-->
    <header class="main-header header-style-five five-alternate">

        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left">
                        <ul class="contact-list clearfix">
                            <li><i class="fa fa-envelope-o"></i> info@tradepickinvetsments.com</li>
                            <li><i class="fa fa-phone"></i><a href="#">+254 722338239</a></li>
                            <li><i class="fa fa-map-marker"></i><a href="#">Enterprise road</a></li>
                        </ul>
                    </div>
                    <div class="top-right">

                        <!--Language-->
                        <div class="language dropdown"><a class="btn btn-default dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#"><span class="flag-icon"><img src="images/icons/flag-icon.png" alt=""/></span>En</a>
                            <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                                <li><a href="#">English</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container">
                <div class="clearfix">

                    <div class="pull-left logo-box">
                        <div class="logo"><a href="/"><img src="/pics/logo.PNG" alt="" title=""></a></div>
                    </div>

                    <div class="nav-outer clearfix">

                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-md" style="background-color: ">
                            <div class="navbar-header">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li  <?php if($_SERVER['SCRIPT_NAME']=="/index.php") { ?>  class="current"   <?php   }?> ><a href="/">Home</a>
                                    </li>
                                    <li <?php if($_SERVER['SCRIPT_NAME']=="/about.php") { ?>  class="current"   <?php   }?> ><a href="/about.php">About Us</a>
                                    </li>
                                    <li <?php if($_SERVER['SCRIPT_NAME']=="/product.php") { ?>  class="current"   <?php   }?> ><a href="/product.php">Our products</a>
                                    </li>
                                    <li <?php if($_SERVER['SCRIPT_NAME']=="/contact.php") { ?>  class="current"   <?php   }?> ><a href="/contact.php">Contact us</a></li>
                                </ul>
                            </div>

                        </nav>

                        <!--Outer Box-->
                        <div class="outer-box">

                            <!--Search Box-->
                            <div class="search-box-outer">
                                <div class="dropdown">
                                    <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                                    <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                        <li class="panel-outer">
                                            <div class="form-container">
                                                <form method="post" action="https://html.themexriver.com/finano/blog.html">
                                                    <div class="form-group">
                                                        <input type="search" name="field-name" value="" placeholder="Search Here" required>
                                                        <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!--Sticky Header-->
        <div class="sticky-header" style="background-color: #1A426E;">
            <div class="auto-container clearfix" style="background-color: #1A426E;">
                <!--Logo-->
                <div class="logo pull-left">
                    <a style="color: white;font-size: 30px;" href="/" class="img-responsive"><img src="" alt="" title=""><i>Tradepick Investments</i></a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md" style="background-color: #1A426E;">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1" style="background-color: #1A426E;">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="/">Home</a>
                                </li>
                                <li ><a href="about.php">About Us</a>
                                </li>
                                <li ><a href="/product.php">Our products</a>
                                </li>
                                <li><a href="/contact.php">Contact us</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>

            </div>
        </div>
        <!--End Sticky Header-->

    </header>
    <!--End Main Header -->
</div>









