<?php
require_once ('navbar.php')
?>
<!--Page Title-->
<section class="page-title" style="background-image:url(/pics/products/bg.jpeg)">
    <div class="auto-container">
        <div class="content">
            <h1>Bumble <span></span></h1>
            <ul class="page-breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Pages</li>
                <li>Our Products</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- Portfolio Single Section -->
<section class="portfolio-single-section">
    <div class="auto-container">
        <!-- Sec Title -->
        <div class="sec-title centered">
            <div class="title">Bumble</div>
            <h3>Our loose drink collections offer an easy way to explore a variety of max fly</h3>
        </div>

        <div class="row clearfix">

            <!-- Image Column -->
            <div class="image-column col-lg-7 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image">
                        <img src="/pics/products/p5.jpeg" alt="" />
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-5 col-md-12 col-sm-12">
                <div class="inner-column">
                    <h3>Bumble</h3>
                    <div class="text">
                        <p>It prevents nappy rash with its silky breathable texture and cottony inner surface so your baby enjoys having fun and moving freely without its skin getting irritated. Extra absorbing thin particles absorbs the moisture and provides a dry surface .</p>

                    </div>

                </div>
            </div>

        </div>

        <!-- Lower Section -->
        <div class="lower-section">
            <div class="row clearfix justify-content-center">

                <!-- Project Block -->
                <div class="project-block  col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p4.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p4.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Bumble</a></h5>
                            <div class="designation">Jumbo packet</div>
                        </div>
                    </div>
                </div>
                <div class="project-block  col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p8.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p8.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Bumble</a></h5>
                            <div class="designation">Hanim Elli Wipes</div>
                        </div>
                    </div>
                </div>



            </div>
        </div>

    </div>
</section>
<!-- End Portfolio Single Section -->









<?php
require_once ('footer.php')
?>
