<?php
require_once ('navbar.php')
?>
<!--Page Title-->
<section class="page-title" style="background-image:url(/pics/products/p16.jpeg)">
    <div class="auto-container">
        <div class="content">
            <h1>Max <span>Fly</span></h1>
            <ul class="page-breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Pages</li>
                <li>Our Products</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- Portfolio Single Section -->
<section class="portfolio-single-section">
    <div class="auto-container">
        <!-- Sec Title -->
        <div class="sec-title centered">
            <div class="title">Max Fly</div>
            <h3>Our loose drink collections offer an easy way to explore a variety of max fly</h3>
        </div>

        <div class="row clearfix">

            <!-- Image Column -->
            <div class="image-column col-lg-7 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image">
                        <img src="/pics/img2.jpeg" alt="" />
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-5 col-md-12 col-sm-12">
                <div class="inner-column">
                    <h3>Max Fly</h3>
                    <div class="text">
                        <p>MAX FLY is an energy drink naturally sweetened with no artificial sweetners that delivers the jolt you crave with 150 mg of caffeine (about a cup and a half of coffee). Additionally, MAX FLY includes only regulated ingredients in its specialized blend of B-vitamins. .</p>
                        <p>Maximum Energy Maximum Life</p>
                    </div>

                </div>
            </div>

        </div>

        <!-- Lower Section -->
        <div class="lower-section">
            <div class="row clearfix justify-content-center">

                <!-- Project Block -->
                <div class="project-block  col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p3.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p3.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/max.php">Max Fly</a></h5>
                            <div class="designation">250 ml 24 pieces carton</div>
                        </div>
                    </div>
                </div>
                <div class="project-block  col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p2.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p2.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/max.php">Max Fly</a></h5>
                            <div class="designation">250 ml 3 pieces</div>
                        </div>
                    </div>
                </div>



            </div>
        </div>

    </div>
</section>
<!-- End Portfolio Single Section -->









<?php
require_once ('footer.php')
?>
