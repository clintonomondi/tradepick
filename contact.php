<?php
require_once ('navbar.php')
?>




    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/8.jpg)">
        <div class="auto-container">
            <div class="content">
                <h1>Contact <span>us</span></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>contact</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Contact Page Section -->
    <section class="contact-page-section">
        <div class="map-section">
            <!--Map Outer-->
            <div class="map-outer">
                <!--Map Canvas-->
                <div class="map-canvas"
                     data-zoom="12"
                     data-lat="-37.817085"
                     data-lng="144.955631"
                     data-type="roadmap"
                     data-hue="#ffc400"
                     data-title="Envato"
                     data-icon-path="images/icons/map-marker.png"
                     data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
                </div>
            </div>
        </div>
        <div class="auto-container">
            <div class="inner-container">
                <h2>GET IN TOUCH WITH US</span></h2>
                <div class="row clearfix">

                    <!-- Info Column -->
                    <div class="info-column col-lg-7 col-md-12 col-sm-12">
                        <div class="inner-column">
                            <div class="text">Please contact us using the information below. For additional information on our management consulting services, please visit the appropriate page on our site.</div>
                            <ul class="list-style-five">
                                <li><span class="icon fa fa-building"></span> National Cereals and Produce Board warehouses Store 13,Enterprise road next to Dt dobie. Po box 51759-00100 Nairobi</li>
                                <li><span class="icon fa fa-fax"></span>  +254 0111796620 <br>  0722338239 / 0796493855</li>
                                <li><span class="icon fa fa-envelope-o"></span>info@tradepickinvetsments.com</li>
                            </ul>
                        </div>
                    </div>

                    <!-- Form Column -->
                    <div class="form-column col-lg-5 col-md-12 col-sm-12">
                        <div class="inner-column">

                            <!--Contact Form-->
                            <div class="contact-form">
                                <form id="submitKasae" method="post" name="submitKasae" onsubmit="return    false"     >

                                    <div class="form-group">
                                        <input type="text" name="subject" value="" placeholder="Message Title" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="name" value="" placeholder="Full name" required>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" name="email" value="" placeholder="Email" required>
                                    </div>


                                    <div class="form-group">
                                        <textarea name="message" placeholder="write.."></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" id="submit" class="theme-btn">Submit</button>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Team Page Section -->

    <!-- Contact Info Section -->
    <section class="contact-info-section" style="background-image:url(images/background/10.jpg)">
        <div class="auto-container">
            <div class="row clearfix justify-content-center">


                <div class="column col-lg-4 col-md-6 col-sm-12">
                    <h4>NAIROBI</h4>
                    <ul class="list-style-six">
                        <li><span class="icon flaticon-map-1"></span>National Cereals and Produce Board warehouses Store 13,Enterprise road next to Dt dobie. Po box 51759-00100 Nairobi </li>
                        <li><span class="icon flaticon-phone-receiver"></span> +254 0111796620 <br>  0722338239 / 0796493855 </li>
                        <li><span class="icon flaticon-e-mail-envelope"></span>info@tradepickinvetsments.com</li>
                    </ul>
                </div>


            </div>
        </div>
    </section>
    <!-- End Contact Info Section -->

    <?php
require_once ('footer.php')

?>