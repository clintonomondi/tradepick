<?php
require_once ('navbar.php')
?>

    <!--Main Slider-->
    <section class="main-slider style-two">
        <div class="main-slider-carousel owl-carousel owl-theme">

            <div class="slide" style="background-image:url(pics/slider/pic2.jpeg)">
                <div class="auto-container">
                    <div class="content alternate">
                        <h1 class="alternate">We have  Something special  <br> <span>  for your refined taste</span></h1>
                        <div class="text alternate">We are customer-focused in all areas of our operations and provide excellent,
                            dependable and reliable supplies to clientele both in the public and private sector!</div>
                        <div class="btn-box">
                            <a href="about.php" class="theme-btn btn-style-two">Read more</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slide" style="background-image:url(/pics/slider/pic3.jpeg)">
                <div class="auto-container">
                    <div class="content alternate">
                        <h1 class="alternate">Maximum Energy   <span>Maximum Life</span></h1>
                        <div class="text alternate">We are customer-focused in all areas of our operations and provide excellent,
                            dependable and reliable supplies to clientele both in the public and private sector!</div>
                        <div class="btn-box">
                            <a href="about.php" class="theme-btn btn-style-two">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide" style="background-image:url(/pics/slider/pic5.jpeg)">
                <div class="auto-container">
                    <div class="content alternate">
                        <h1 class="alternate">Maximum Energy   <span>Maximum Life</span></h1>
                        <div class="text alternate">We are customer-focused in all areas of our operations and provide excellent,
                            dependable and reliable supplies to clientele both in the public and private sector!</div>
                        <div class="btn-box">
                            <a href="about.php" class="theme-btn btn-style-two">Read more</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <!-- Services Section Three-->
    <section class="services-section-three">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Services Block -->
                <div class="services-block-three style-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow bounceIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-bar-chart"></span>
                        </div>
                        <h6><a href="#">Our Vision</a></h6>
                        <div class="text">To became the most excellent source of satisfaction by offering products of indisputable quality to our customers in Kenya and beyond.</div>
                    </div>
                </div>

                <!-- Services Block -->
                <div class="services-block-three style-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow bounceIn" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-board"></span>
                        </div>
                        <h6><a href="#">Our Mission</a></h6>
                        <div class="text">We develop a mirror that gives utmost satisfaction to <br>our customers by creating<br> value to their needs.</div>
                    </div>
                </div>

                <!-- Services Block -->
                <div class="services-block-three style-two col-lg-4 col-md-12 col-sm-12">
                    <div class="inner-box wow bounceIn" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-student"></span>
                        </div>
                        <h6><a href="#">Core values</a></h6>
                        <div class="text">We understand that everyone’s requirement is different so we provide a diverse range of products to fit all requirements.</div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- About Section Two -->
    <section class="about-section-two">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Video Column -->
                <div class="video-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <!--Video Box-->
                        <div class="video-box">
                            <figure class="image">
                                <img src="/pics/img2.jpeg" alt="">
                            </figure>
                            <a href="#" class="lightbox-image overlay-box"><span class="flaticon-play-button"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Content Column -->
                <div class="content-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <h3>Max Fly <span>Maximum Energy   maximum Life</span></h3>
                        <div class="text">
                            <p>MAX FLY is an energy drink naturally sweetened with no artificial sweetners that delivers the jolt you crave with 150 mg of caffeine (about a cup and a half of coffee). </p>
                            <p>Additionally, MAX FLY includes only regulated ingredients in its specialized blend of B-vitamins...</p>
                        </div>
                        <a href="/about.php" class="theme-btn btn-style-three">About us</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Services Section Four-->
    <section class="services-section-four">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title">
                <div class="clearfix">
                    <div class="pull-left">
                        <h2>We serve as the best <br> services <span>planner</span></h2>
                    </div>
                    <div class="pull-right">
                        <div class="text">Bellow are other top products available both in wholesale and retail .</div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">

                <!-- Services Block Four -->
                <div class="services-block-four col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="/pics/img1.jpeg" alt="">
                            <div class="overlay-box">
                                <div class="content">
                                    <div class="icon-box">

                                    </div>
                                    <h6>Max Fly 250ml  3pack</h6>
                                </div>
                            </div>

                            <div class="overlay-box-two">
                                <div class="overlay-inner-two">
                                    <div class="content">
                                        <div class="icon-box">

                                        </div>
                                        <h6><a href="max.php">Max Fly 250ml  3pack</a></h6>
                                        <div class="text">Click to read more</div>
                                        <a class="read-more" href="max.php">Let’s start <span class="fa fa-angle-double-right"></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Services Block Four -->
                <div class="services-block-four col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="/pics/products/p8.jpeg" alt="">
                            <div class="overlay-box">
                                <div class="content">
                                    <div class="icon-box">

                                    </div>
                                    <h6>Bumble Baby wipes</h6>
                                </div>
                            </div>

                            <div class="overlay-box-two">
                                <div class="overlay-inner-two">
                                    <div class="content">
                                        <div class="icon-box">

                                        </div>
                                        <h6><a href="bumble.php">Bumble Baby wipes</a></h6>
                                        <div class="text">Click ti read more</div>
                                        <a class="read-more" href="bumble.php">Let’s start <span class="fa fa-angle-double-right"></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Services Block Four -->
                <div class="services-block-four col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="/pics/slider/milk.jpeg" alt="" style="width: 85%">
                            <div class="overlay-box">
                                <div class="content">
                                    <div class="icon-box">

                                    </div>
                                    <h6>Inyage Milk </h6>
                                </div>max
                            </div>

                            <div class="overlay-box-two">
                                <div class="overlay-inner-two">
                                    <div class="content">
                                        <div class="icon-box">

                                        </div>
                                        <h6><a href="/bumble.php">Inyange  Milk</a></h6>
                                        <div class="text">Click to view</div>
                                        <a class="read-more" href="/bumble.php">Let’s start <span class="fa fa-angle-double-right"></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="btn-box text-center">
                <a href="/product.php" class="theme-btn btn-style-two">More products</a>
            </div>

        </div>
    </section>
    <!-- End Services Section Four-->
    <!-- Business Section -->
    <section class="business-section">
        <div class="outer-container">
            <div class="clearfix sticky-container">

                <!-- Title Column -->
                <div class="title-column">
                    <div class="inner-column sticky-box">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="/"><img src="/pics/logo.PNG" alt="" title=""></a>
                        </div>
                        <h2>Why you choose us</span></h2>
                        <div class="text">The company was established to provide quality products and services to organizations and companies in the most efficient way..</div>
                    </div>
                </div>

                <!-- Contents Column -->
                <div class="contents-column">
                    <div class="inner-column">
                        <!-- Title Box -->
                        <div class="title-box">
                            <h2>Max Fly is the latest sensation in town.</span></h2>
                        </div>

                        <!-- Services Outer -->
                        <div class="services-blocks">

                            <!-- Services Block Five -->
                            <div class="services-block-fourteen">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-tick"></span>
                                    </div>
                                    <h6><a href="#">100% Natural</a></h6>
                                    <div class="text">We develop a mirror that gives utmost satisfaction to our customers by creating value to their needs.</div>
                                </div>
                            </div>

                            <!-- Services Block Five -->
                            <div class="services-block-fourteen">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-tick"></span>
                                    </div>
                                    <h6><a href="#">Free delivery</a></h6>
                                    <div class="text">We provide free delivery of Max Fly energy  drink,bumble and other products any where.</div>
                                </div>
                            </div>

                            <!-- Services Block Five -->
                            <div class="services-block-fourteen">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-tick"></span>
                                    </div>
                                    <h6><a href="#">Quality product</a></h6>
                                    <div class="text">The company was established to provide quality products and services to organizations and companies in the most efficient way.</div>
                                </div>
                            </div>

                        </div>

                        <!-- Testimonial Boxed -->
                        <div class="testimonial-boxed" style="background-image:url(/pics/img1.jpeg)">
                            <div class="inner-boxed">
                                <div class="inner-content">
                                    <h2>What Clients say <span>about Max Fly Drink</span></h2>
                                    <div class="single-item-carousel owl-carousel owl-theme">

                                        <!-- Testimonial Block Two -->
                                        <div class="testimonial-block-two">
                                            <div class="inner-box">
                                                <div class="content-box">
                                                    <div class="text">I am constantly in motion and want a drink that will give me Energy when I need it.</div>
                                                </div>
                                                <div class="lower-box">
                                                    <div class="lower-inner">
<!--                                                        <div class="image"><img src="images/resource/author-4.jpg" alt="" title=""></div>-->
                                                        <h3>Omar Ali</h3>
<!--                                                        <div class="designation"> Director Habco distributors</div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Testimonial Block Two -->
                                        <div class="testimonial-block-two">
                                            <div class="inner-box">
                                                <div class="content-box">
                                                    <div class="text">I do like Energy fulls on any busy day.That is why i go for energy drink, Max Fly my choice</div>
                                                </div>
                                                <div class="lower-box">
                                                    <div class="lower-inner">
<!--                                                        <div class="image"><img src="images/resource/author-4.jpg" alt="" title=""></div>-->
                                                        <h3>Njoroge Peter</h3>
<!--                                                        <div class="designation">Founder & CEO</div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Testimonial Block Two -->
                                        <div class="testimonial-block-two">
                                            <div class="inner-box">
                                                <div class="content-box">
                                                    <div class="text">Max Fly drink helps me keep awake when am working on a project.</div>
                                                </div>
                                                <div class="lower-box">
                                                    <div class="lower-inner">
<!--                                                        <div class="image"><img src="images/resource/author-4.jpg" alt="" title=""></div>-->
                                                        <h3>Ashad Kumar</h3>
                                                        <div class="designation">Founder & CEO</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="testimonial-block-two">
                                            <div class="inner-box">
                                                <div class="content-box">
                                                    <div class="text">Very comfortable and dryness for the baby.</div>
                                                </div>
                                                <div class="lower-box">
                                                    <div class="lower-inner">
                                                        <!--                                                        <div class="image"><img src="images/resource/author-4.jpg" alt="" title=""></div>-->
                                                        <h3>Ashad Kumar</h3>
                                                        <div class="designation">Founder & CEO</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Awards Section -->
                        <div class="awards-blocks">
                            <div class="awards-inner">
                                <h2>Payment <span>Services</span> </h2>
                                <div class="text">Payment can also be done through Lipa Na M-pesa upon delivery of the product .</div>
                                <ul class="clearfix">
                                    <li><img src="pics/mpesa.jpeg" alt="" title=""></li>
                                    <li><img src="pics/mpesa.jpeg" alt="" title=""></li>
                                    <li><img src="pics/mpesa.jpeg" alt="" title=""></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Business Section -->
<?php
require_once ('footer.php')

?>