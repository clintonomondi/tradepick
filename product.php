<?php
require_once ('navbar.php')
?>

<!--Page Title-->
<section class="page-title" style="background-image:url(/pics/products/bg.jpeg)">
    <div class="auto-container">
        <div class="content">
            <h1>Our <span>Products</span></h1>
            <ul class="page-breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Pages</li>
                <li>Our Products</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Portfolio Section Three-->
<section class="portfolio-section-three">
    <div class="auto-container">
        <!-- Sec Title -->
        <div class="sec-title centered">
            <div class="title">Our Products</div>
            <h3>At our store, we have a A wide range of max fly and bumble baby diapers and wet wipes to serve you!.</h3>
            <p>Our Max fly Energy drink guarantees you Premium flavour and Maximum Energy</p>
            <p>Bumble baby diaper ensures comfort and dryness for your baby</p>
        </div>

        <!--MixitUp Galery-->
        <div class="mixitup-gallery">

            <!--Filter-->
            <div class="filters clearfix">

                <ul class="filter-tabs filter-btns text-center clearfix">
                    <li class="active filter" data-role="button" data-filter="all">All</li>
                    <li class="filter" data-role="button" data-filter=".max">Max Fly</li>
                    <li class="filter" data-role="button" data-filter=".bumble">Bumble</li>
                    <li class="filter" data-role="button" data-filter=".milk">Other products</li>
                </ul>

            </div>

            <div class="filter-list row clearfix">

                <!-- Project Block -->
                <div class="project-block mix max col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p3.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p3.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/max.php">Max Fly</a></h5>
                            <div class="designation">250 ml 24 pieces carton</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix max col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p2.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p2.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/max.php">Max Fly</a></h5>
                            <div class="designation">250 ml 3 pieces</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix bumble col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p4.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p4.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Bumble</a></h5>
                            <div class="designation">Jumbo packet</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix bumble col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p8.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p8.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Bumble</a></h5>
                            <div class="designation">Hanim Elli Wipes</div>
                        </div>
                    </div>
                </div>

                <div class="project-block mix max col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/slider/pic3.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p8.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Max Fly</a></h5>
                            <div class="designation">250 ml</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix max col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/slider/pic4.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/slider/pic4.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Max Fly</a></h5>
                            <div class="designation">250 ml</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix milk col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/milk.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/milk.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Inyange Milk</a></h5>
                            <div class="designation"></div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix milk col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/milk2.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/milk2.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Inyange Milk</a></h5>
                            <div class="designation"></div>
                        </div>
                    </div>
                </div>














                <div class="project-block mix max col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p6.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p6.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/max.php">Max Fly</a></h5>
                            <div class="designation">250 ml 4 pieces</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix max col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p14.jpeg" alt="" />
                            <div class="overlay-box">
                                <a href="/pics/products/p14.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/max.php">Max Fly</a></h5>
                            <div class="designation">250 ml </div>
                        </div>
                    </div>
                </div>


                <div class="project-block mix bumble col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p12.jpeg" alt="" style="width: 80%" />
                            <div class="overlay-box">
                                <a href="/pics/products/p12.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="bumble.php">Bumble</a></h5>
                            <div class="designation">Low counts 3-6 Kg</div>
                        </div>
                    </div>
                </div>
                <div class="project-block mix bumble col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="/pics/products/p10.jpeg" alt="" style="width: 80%" />
                            <div class="overlay-box">
                                <a href="/pics/products/p10.jpeg" data-fancybox="gallery-3" data-caption="" class="plus flaticon-plus-symbol"></a>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h5><a href="/bumble.php">Bumble</a></h5>
                            <div class="designation">Low counts 2-5 Kg</div>
                        </div>
                    </div>
                </div>




            </div>
        </div>

        <!-- Button Box -->
        <div class="button-box text-center">
            <a href="/about.php" class="theme-btn btn-style-fifteen">Learn more</a>
        </div>

    </div>
</section>
<!-- End Case Section Three-->














<?php
require_once ('footer.php')
?>
